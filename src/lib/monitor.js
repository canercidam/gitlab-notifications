'use strict';

const $ = require('jquery');
const _gaq = require('./ga');
const config = require('../config');
_gaq.push(['_setAccount', config.GA_ID]);

const EXTENSION_VERSION = require('../manifest.json').version;
function errorLogger(message, file, line) {
  const msg = file + '#' + line + ':' + message;
  _gaq.push(['_trackEvent', msg, 'errors', EXTENSION_VERSION]);
}

const TIMES = {};

const monitor = {
  error: function(err, msg) {
    let message = err.message || err.description || '';
    if (msg) {
      message = msg + '-' + message;
    }
    const fileName = err.filename || err.fileName || err.sourceURL || '';
    const line = err.lineno || err.lineNumber || err.line || 0;
    errorLogger(message, fileName, line);
  },
  pageview: () => {
    _gaq.push(['_trackPageview']);
  },
  log: (seed, profile = 'log') => {
    _gaq.push(['_trackEvent', seed, profile]);
  },
  click: (seed) => {
    _gaq.push(['_trackEvent', seed, 'click']);
  },
  calc: (seed, value) => {
    if (typeof value !== 'number') return;
    _gaq.push(['_trackEvent', seed, 'calc', '', value, true]);
  },

  time: function(name) {
    TIMES[name] = Date.now();
  },
  timeEnd: function(name) {
    if (!TIMES[name]) return -1;
    const time = Date.now() - TIMES[name];
    TIMES[name] = undefined;
    _gaq.push(['_trackEvent', name, 'calc', '', time, true]);
  },
};


window.onerror = function(message, file, line) {
  errorLogger(message, file, line);
};

$(document).on('mousedown', '[seed]', function(evt) {
  const seed = $(this).attr('seed');
  if (seed) {
    monitor.click(seed);
  }
  evt.stopPropagation();
});

module.exports = monitor;
