
# GitLab Notifications

GitLab Notifications Center.

## Installation

- [Chrome Web Store](https://chrome.google.com/webstore/detail/gitlab-notifications/neidmbjigjejpekbknfbmcgmkbfgmfmi)

## Features

* Notifications Center
  * Assign to me.
  * Metion to me.
* Issue:
  * Assign to me.
  * Create by me.
  * Metion to me.
* Merge Request:
  * Assign to me.
  * Create by me.
  * Metion to me.
* Others
  * `@id` to `name (@id)`
